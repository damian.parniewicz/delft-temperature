#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import bs4  # type: ignore
import dryscrape  # type: ignore
import os
import re
import requests
import sys
import time
import webkit_server  # type: ignore
from dataclasses import dataclass
from typing import Callable, Any

WEATHER_URL = os.getenv("WEATHER_URL", "https://www.weerindelft.nl")


class TempAppExc(Exception):
    pass


class WeatherServerProblem(TempAppExc):
    pass


class TemperatureNotFound(TempAppExc):
    pass


def get_html(url: str) -> bytes:
    """Get a html page for a given `url`"""
    try:
        return requests.get(url=url).content
    except requests.ConnectionError as e:
        raise WeatherServerProblem(f"Cannot connect to weather server: {url}")


def dryscrape_visit(url: str, sess: Any) -> None:
    """Visit page `url` using a dryscape session `sess`"""
    try:
        sess.visit(url)
    except webkit_server.InvalidResponseError:
        raise WeatherServerProblem(f"Cannot connect to weather server: {url}")


def get_ajax_html(url: str) -> bytes:
    """Get a html page for a given `url` and wait a moment for updates provided by AJAX"""
    dryscrape.start_xvfb()
    sess = dryscrape.Session()
    dryscrape_visit(url, sess)
    time.sleep(1)  # wait for ajax to dynamicly update the page
    dryscrape_visit(url, sess)
    return sess.body()


def find_temperature_frame_url(html_doc: bytes) -> str:
    """Find a html frame url with the temperature panel in `html_doc`"""
    soup = bs4.BeautifulSoup(html_doc, "html.parser")
    frame = soup.find(id="ifrm_3")
    if frame is None:
        raise TemperatureNotFound("No html frame with temperature")
    return frame.attrs["src"]


def find_temperature_span(html_doc: bytes) -> str:
    """Find a html span of the ajaxtemp object in `html_doc`"""
    soup = bs4.BeautifulSoup(html_doc, "html.parser")
    span = soup.find(id="ajaxtemp")
    if span is None:
        raise TemperatureNotFound("No html span with temperature")
    return span.text


CELCIUS_SYMBOL = b"\xc2\xb0C".decode()


def find_temperature_value(text: str) -> float:
    """Find a temperature in `text` which is a float number followed by Celsius symbol"""
    temp = text.strip()
    if not temp.endswith(CELCIUS_SYMBOL):
        raise TemperatureNotFound("No Celcius symbol found")
    temp = temp.replace(CELCIUS_SYMBOL, "")

    try:
        temp_float = float(temp)
    except ValueError as e:
        raise TemperatureNotFound("No correct temperature value found")
    return round(temp_float)


@dataclass
class Infrastructure:
    """App infrastructure composed of datasource functions"""

    get_html: Callable[[str], bytes]
    get_ajax_html: Callable[[str], bytes]


def get_temperature(weather_url: str, infra: Infrastructure) -> float:
    """
    Get temperature

    Get a current temperature from a weather portal accesible via `weather_url`
    using `infra` datasource infrastructure.
    """
    html_doc = infra.get_html(weather_url)
    frame_url = find_temperature_frame_url(html_doc)

    html_doc = infra.get_ajax_html(frame_url)
    span_text = find_temperature_span(html_doc)

    temperature = find_temperature_value(span_text)
    return temperature


def delft_temperature() -> float:
    """Get a current temperature in the Delft city"""
    infra = Infrastructure(get_html=get_html, get_ajax_html=get_ajax_html)
    return get_temperature(WEATHER_URL, infra)


def main() -> None:
    try:
        temp = delft_temperature()
    except TempAppExc as e:
        print(f"Failure: {e}")
        sys.exit(-1)

    print(f"{temp} degrees Celcius")


if __name__ == "__main__":
    main()
