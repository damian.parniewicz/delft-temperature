# delft-temperature

Retrieves from http://www.weerindelft.nl/ the current temperature in Delft and prints it to standard output, rounded to degrees Celcius.

## Running

Please run the application using docker:

```bash
docker build . -t delft_temperature
docker run delft_temperature
```

In order to change weather server URL please set WEATHER_URL environment variable:

```bash
docker run -e "WEATHER_URL=https://www.new-weerindelft.nl" delft_temperature
```




