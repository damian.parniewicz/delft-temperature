FROM python:3.9-slim
ENV PYTHONUNBUFFERED 1

RUN apt update -y && apt install -y libqt5webkit5-dev build-essential xvfb

RUN pip install --upgrade pip
RUN pip install --no-cache-dir \ 
    beautifulsoup4==4.10.0 \
    dryscrape==1.0 \
    requests==2.25.1

COPY . /app
WORKDIR /app

ENTRYPOINT ["python", "HoeWarmIsHetInDelft.py"]

